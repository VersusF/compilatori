(*
Define a datatype representing arithmetic expressions with constants, 
variables, sum and product. Define an evaluator for expressions of this type.
*)

datatype Aexp = const of int
				| var of int
				| add of Aexp*Aexp
				| times of Aexp*Aexp;

fun evaluator(const(n)) = evaluator(n)
	| evaluator(var(n)) = evaluator(n)
	| evaluator(add(x, y)) = add(evaluator(x), evaluator(y))
	| evaluator(times(x, y)) = times(evaluator(x), evaluator(y))
	| evaluator(n) = n;

fun add(x, y) = x + y;
fun times(x, y) = x * y;