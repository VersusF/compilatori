(*
Define a datatype `btree’ representing binary trees.

Then write the following functions:

occ: btree * int → bool

This function decides whether an integer occurs in a tree or not.

sm: btree → int

This function returns the sum of all the labels of a binary tree of integers.
*)
load "Int";
datatype btree = leaf of int | btree of int*btree*btree

val padre = btree(4,
		btree(2,
			leaf(1),
			leaf(3)
		),
		leaf(5)
	);

fun occ(n, leaf(m)) = (n=m)
	| occ(n, btree(m, left, right)) = (n=m) orelse occ(n, left) orelse occ(n, right);

fun sm(btree(n, left, right)) = n + sm(left) + sm(right)
	| sm(leaf(n)) = n;

fun printFormatted(btree(n, left, right), tab) = 
	printFormatted(right, tab ^ "  ") ^ tab ^ Int.toString n ^ "\n" ^ printFormatted(left, tab ^ "  ")
	| printFormatted(leaf(n), tab) = tab ^ Int.toString n ^ "\n";

fun printTree(leaf(n)) = Int.toString n ^ "\n"
	| printTree(btree(n, left, right)) = printFormatted(btree(n, left, right), "");


print(printTree(padre));