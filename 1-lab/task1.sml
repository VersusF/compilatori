val x = 3;
val x = 8;
val list = ["apple", "orange", "pear", "grapefruit"];
(*Estraggo la stringa pear*)
hd (tl (tl (list)));
(*provo questi comandi*)
1::[2,3]; 
[1,5]@[2,3];
(*il :: è un append, il @ concatena*)
length ["plato","socrates","aristotle"]; 
rev ["plato","socrates","aristotle"];
(*length mi da la lunghezza, rev fa il reverse*)

(*definiamo funzioni*)
fn x => x+1;
val increment =
	fn x => x+1;
increment x;

fun addOne x = x+1;

(*variabili*)

let
	val n = 8
in
	n*n
end;

(*Recursion*)
fun fibonacciList(list, n) =
	if n < 1 then
		list
	else
		fibonacciList(hd(list)+hd(tl(list))::list, n-1);

fun fibonacci(n) =
	fibonacciList([1, 1], n-2);

fibonacci(10);

(*keep first n elements*)
fun keepFirstN(n, list) =
	if n = 0 then
		nil
	else
		hd(list)::keepFirstN(n-1, tl(list));

keepFirstN(3,["a","b","c","d","e","f","g","h","i"]); 

(*Pattern matching*)
fun mystery([]) = [] 
  | mystery(start::rest) = 
      mystery(rest) @ [start:int];

fun keepFirstNPattern(0, _) = []
	| keepFirstNPattern(n, primo::resto) = primo::keepFirstNPattern(n-1, resto)
	| keepFirstNPattern(n, []) = [];

keepFirstNPattern(3,["a","b","c","d","e","f","g","h","i"]);

(*Iteration and printing*)
load "Int";

let fun loop(count) = 
 ( 
  print("Count = "); 
  print(Int.toString(count)); 
  print("\n"); 
  if count < 10 then 
      loop(count+1) 
  else 
      count 
  ) 
in 
    loop(0) 
end;